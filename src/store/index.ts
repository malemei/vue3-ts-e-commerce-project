import useCartStore from './modules/cart';
import useHomeStore from './modules/home';
import useMemberStore from './modules/member';
import useOrderStore from './modules/order';

const useStore = () => {
  return {
    home: useHomeStore(),
    member: useMemberStore(),
    cart: useCartStore(),
    order: useOrderStore(),
  };
};

export default useStore;
