import type { Profile } from '@/types';
import { http } from '@/utils/request';
import { defineStore } from 'pinia';
import { message } from '@/components/XtxUI';
// 🎯非 vue 组件，导入路由实例
import router from '@/router';
import useStore from '..';

const useMemberStore = defineStore('member', {
  persist: true,
  // 状态
  state: () => ({
    // 用户信息
    profile: {} as Profile,
  }),
  // 计算
  getters: {
    // 是否登录
    isLogin(): boolean {
      return Boolean(this.profile.token);
    },
  },
  // 方法
  actions: {
    // 登录后续
    loginSuccess(profile: Profile) {
      this.profile = profile;
      message({ type: 'success', text: '登录成功' });
      // router.push('/');
      // 由于是ts文件，是非组件，获取路由信息需要通过路由实例的currentRoute 获取
      const { target = '/' } = router.currentRoute.value.query;
      router.push(target as string);

      // 合并未登录的购物车商品
      const { cart } = useStore();
      cart.mergeLocalCart();
    },

    // 用户名和密码登录
    async login(data: { account: string; password: string }) {
      const res = await http<Profile>('POST', '/login', data);
      /*  // 1. 保存用户信息到 state 中
      this.profile = res.data.result;
      // 2. 请求成功给用户提示
      message({ type: 'success', text: '登录成功' });
      // 3. 跳转页面
      router.push('/'); */
      this.loginSuccess(res.data.result);
    },
    LoginOut() {
      this.profile = {} as Profile;
      message({ type: 'success', text: '退出登录成功' });
    },

    // 三方登录_账号绑定  /login/social/bind
    async loginBind(data: { unionId: string; mobile: string; code: string }) {
      // 发送请求
      const res = await http<Profile>('POST', '/login/social/bind', data);
      // 调用登录成功后的逻辑
      // console.log(res);
      this.loginSuccess(res.data.result);
    },

    // 三方登录-直接登录
    async loginSocial(data: object) {
      const res = await http<Profile>('POST', '/login/social', data);
      this.loginSuccess(res.data.result);
    },

    // 获取三方登录验证码  /login/social/code
    async getSocialCode(mobile: string) {
      // 🚨注意接口：三方登录_发送已有账号短信
      await http('GET', '/login/social/code', { mobile: mobile });
      // 🔔温馨提醒：验证码是发送到用户手机上的
      message({ type: 'success', text: '验证码发送成功' });
    },
  },
});

export default useMemberStore;
