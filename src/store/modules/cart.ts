import { http } from '@/utils/request';
import { defineStore } from 'pinia';
import type { CartItem, CartList } from '@/types';
import { message } from '@/components/XtxUI';
import useStore from '..';
const useCartStore = defineStore('cart', {
  // 开启持久化本地存储
  persist: true,
  // 状态
  state: () => ({
    // 购物车列表
    list: [] as CartList,
  }),
  // 计算
  getters: {
    // 做一层数据中转
    isMemberLogin(): boolean {
      const { member } = useStore();
      return member.isLogin;
    },
    // 计算有效商品列表 isEffective = true  filter
    effectiveList(): CartList {
      return this.list.filter((item) => item.stock > 0 && item.isEffective);
    },

    // forEach写法
    /*  effectiveListCount(): number {
      this.list.forEach((item) => {
        this.countNum += item.count;
      });
      return this.countNum;
    }, */
    // reduce写法：有效商品总数量 把effctiveList中的每一项的count叠加起来
    effectiveListCount(): number {
      return this.effectiveList.reduce((sum, item) => sum + item.count, 0);
    },
    // 总钱数  = 所有单项的钱数累加  单项的钱数 = 数量 * 单价
    effectiveListPrice(): string {
      let sum = 0;
      this.effectiveList.forEach((item) => {
        if (item.selected) sum += item.count * Number(item.nowPrice);
      });
      return sum.toFixed(2);
    },
    // 计算全选状态
    isAllSelected(): boolean {
      return (
        this.effectiveList.length > 0 &&
        this.effectiveList.every((v) => v.selected)
      );
    },
    //计算选择的商品
    isAllCount(): number {
      let sum = 0;
      this.effectiveList.forEach((item) => {
        if (item.selected) sum += item.count;
      });
      return sum;
    },
  },
  // 方法
  actions: {
    // 加入购物车
    async addCart(data: CartItem) {
      if (this.isMemberLogin) {
        // 已登录，调用接口
        const res = await http('POST', '/member/cart', {
          skuId: data.skuId,
          count: data.count,
        });

        // 加入成功后，主动获取服务器最新列表
        this.getCartList();
      } else {
        // 未登录情况 - 操作本地数据(相当于高级版todos)
        // 添加商品分两种情况：
        // find返回查找的目标对象
        const cartItem = this.list.find((item) => item.skuId === data.skuId);

        if (cartItem) {
          // 情况1：已添加过的的商品，累加数量即可
          cartItem.count += data.count;
        } else {
          // 情况2：新添加的商品，前添加到数组中
          this.list.unshift(data);
        }
        //未登录，存储本地
      }
      // 成功提示
      message({ type: 'success', text: '加入购物车成功' });
    },
    // 获取购物车列表
    async getCartList() {
      // 已登录，调用接口
      if (this.isMemberLogin) {
        const res = await http<CartList>('GET', '/member/cart');
        // 保存购物车列表数据
        this.list = res.data.result;
      } else {
        // 🚨未登录情况，首次打开时需查询购物车列表商品最新价格，库存信息
        this.list.forEach(async (item) => {
          const { skuId } = item;
          // 根据 skuId 获取最新商品信息
          const res = await http<CartItem>('GET', `/goods/stock/${skuId}`);
          // 保存最新商品信息
          const newSkuInfo = res.data.result;
          // 更新商品现价
          item.nowPrice = newSkuInfo.nowPrice;
          // 更新商品库存
          item.stock = newSkuInfo.stock;
          // 更新商品是否有效
          item.isEffective = newSkuInfo.isEffective;
        });
      }
    },
    // 删除购物车商品
    async deleteCart(ids: string[]) {
      if (this.isMemberLogin) {
        const res = await http('DELETE', '/member/cart', { ids });
        // 🎯主动获取最新购物车列表
        this.getCartList();
      } else {
        this.list = this.list.filter((item) => item.skuId !== ids[0]);
      }
      // 提示
      message({ type: 'success', text: '删除成功' });
    },
    // 修改购物车商品(是否选中,商品数量)
    async updateCart(
      skuId: string,
      data: { selected?: boolean; count?: number }
    ) {
      if (this.isMemberLogin) {
        // 已登录，调用接口
        const res = await http('PUT', `/member/cart/${skuId}`, data);
        // 主动获取最新列表
        this.getCartList();
      } else {
        //未登录时功能
        // 查找list中对应的对象
        const cartItem = this.list.find((v) => v.skuId === skuId);
        const { count, selected } = data;
        if (cartItem) {
          if (count !== undefined) cartItem.count = count;
          if (selected !== undefined) cartItem.selected = selected;
        }
      }
    },
    // 购物车全选/取消全选
    async updateCartAllSelected(data: { selected: boolean; ids?: string[] }) {
      if (this.isMemberLogin) {
        // 已登录，调用接口
        const res = await http('PUT', '/member/cart/selected', data);
        // 获取购物车列表
        this.getCartList();
      } else {
        this.list.forEach((item) => {
          item.selected = data.selected;
        });
      }
    },
    // 合并购物车
    async mergeLocalCart() {
      // 映射接口所需参数 ,map 过滤后得到一个对象{ skuId, selected, count }数组
      const data = this.list.map(({ skuId, selected, count }) => ({
        skuId,
        selected,
        count,
      }));

      // 调用合并购物车的接口
      await http('POST', '/member/cart/merge', data);
      // 合并成功，重新获取购物车列表
      this.getCartList();
    },
  },
});

export default useCartStore;
