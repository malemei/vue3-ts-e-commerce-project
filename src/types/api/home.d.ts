// 分类数据单项类型
export interface Goods {
  desc: string;
  id: string;
  name: string;
  picture: string;
  price: string;
  title: string;
  alt: string;
}

export interface Children {
  id: string;
  name: string;
  picture: string;
  goods: Goods[];
}

export interface Category {
  id: string;
  name: string;
  picture: string;
  children: Children[];
  goods: Goods[];
}

// 分类数据列表类型
export type CategoryList = Category[];

// 轮播图类型
export interface Banner {
  id: string;
  imgUrl: string;
  hrefUrl: string;
  type: string;
}

export type BannerList = Banner[];

// 🔔电商网站商品的类型基本一致，可以复用
export interface Goods {
  id: string;
  name: string;
  desc: string;
  price: string;
  picture: string;
  orderNum: number;
}

// 商品列表类型(可以复用)
export type GoodsList = Goods[];
