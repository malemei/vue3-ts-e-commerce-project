// components.d.ts

import XtxSkeleton from './Skeleton/Skeleton.vue';
import XtxButton from './Button/index.vue';
import XtxSlider from './Slider/index.vue';
import XtxMore from './More/index.vue';
import XtxBread from './Bread/index.vue';
import XtxBreadItem from './Bread/Item.vue';
import XtxCity from './City/index.vue';
import XtxCount from './Count/index.vue';
import XtxCheckBox from './CheckBox/index.vue';
import XtxImageView from './ImageView/index.vue';
import XtxSku from './Sku/index.vue';
import XtxDialog from './Dialog/index.vue';
import XtxInfiniteLoad from './InfiniteLoad/index.vue';
import XtxTabs from './Tabs/index.vue';
import XtxTabPane from './Tabs/pane.vue';

// declare            声明
// module             模块
// GlobalComponents   全局组件
// 🎉功能：为 vue 模块提供全局组件的类型声明
declare module 'vue' {
  export interface GlobalComponents {
    // 自己的全局组件注册类型声明, typeof 表示基于获取组件的TS类型
    XtxButton: typeof XtxButton;
    XtxSkeleton: typeof XtxSkeleton;
    XtxSlider: typeof XtxSlider;
    XtxMore: typeof XtxMore;
    XtxBread: typeof XtxBread;
    XtxBreadItem: typeof XtxBreadItem;
    XtxCity: typeof XtxCity;
    XtxCount: typeof XtxCount;
    XtxCheckBox: typeof XtxCheckBox;
    XtxImageView: typeof XtxImageView;
    XtxSku: typeof XtxSku;
    XtxDialog: typeof XtxDialog;
    XtxInfiniteLoad: typeof XtxInfiniteLoad;
    XtxTabs: typeof XtxTabs;
    XtxTabPane: typeof XtxTabPane;
  }
}

export {};
