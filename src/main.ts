import { createApp } from 'vue';
import App from './App.vue';
import 'normalize.css';
// 公共样式导入
import '@/assets/styles/common.less';
// 导入路由
import router from './router';
// 创建pinia实例
import { createPinia } from 'pinia';
// 导入组件库，全局注册组件库
import XtxUI from '@/components/XtxUI';

import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';
const pinia = createPinia();
// 安装pinia持久化存储插件
pinia.use(piniaPluginPersistedstate);
const app = createApp(App);
app.use(XtxUI);
app.use(pinia);
app.use(router);
app.mount('#app');
