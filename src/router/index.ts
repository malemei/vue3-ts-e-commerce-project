import { message } from '@/components/XtxUI';
import useStore from '@/store';
import { createRouter, createWebHashHistory } from 'vue-router';
const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      component: () => import('@/views/Layout/index.vue'),
      children: [
        {
          path: '/',
          component: () => import('@/views/Home/index.vue'),
        },
        {
          path: '/category/:id',
          component: () => import('@/views/Category/index.vue'),
        },
        {
          path: '/goods/:id',
          component: () => import('@/views/Goods/index.vue'),
        },
        {
          path: '/cart',
          component: () => import('@/views/Cart/index.vue'),
        },
        {
          path: '/member/checkout',
          component: () => import('@/views/Checkout/index.vue'),
        },
        {
          path: '/member/pay',
          component: () => import('@/views/Pay/index.vue'),
        },
        {
          path: '/pay/callback',
          component: () => import('@/views/Pay/callback.vue'),
        },
        {
          path: '/member',
          component: () => import('@/views/Member/Layout/index.vue'),
          children: [
            {
              path: '',
              component: () => import('@/views/Member/Home/index.vue'),
            },
            {
              path: 'order',
              component: () => import('@/views/Member/Order/index.vue'),
            },
          ],
        },
      ],
    },
    {
      path: '/login',
      component: () => import('@/views/Login/index.vue'),
    },
    {
      path: '/login/callback',
      component: () => import('@/views/Login/callback.vue'),
    },
  ],
  // 始终滚动到顶部
  scrollBehavior: () => {
    return { top: 0, behavior: 'smooth' };
  },
});
router.beforeEach((to) => {
  const { member } = useStore();
  // startsWith方法，就是问你你写的字符串是以什么字符开头，返回值为布尔值
  if (to.path.startsWith('/member') && !member.isLogin) {
    message({ type: 'warn', text: '请您先登录' });
    return `/login?target=${to.fullPath}`;
  }
});
export default router;
