import { useIntersectionObserver, useIntervalFn } from '@vueuse/core';
import dayjs from 'dayjs';
import { computed, ref } from 'vue';

export function useObserver(ApiFn: () => void) {
  // 目标元素，用于绑定模板ref
  const target = ref(null);
  // 检测目标元素是否进入可视区
  const { stop } = useIntersectionObserver(target, ([{ isIntersecting }]) => {
    // 如果目标元素进入可视区
    if (isIntersecting) {
      // 调用 action 发完请求
      ApiFn();
      // 请求发送后，停止检测的函数，不用再检测了
      stop();
    }
  });
  // 记得返回目标元素
  return { target };
}

// 封装倒计时业务
export function useCountDown() {
  // 倒计时初始值
  const count = ref(0);
  // 根据传入的秒数计算出格式化后的时间
  const countTimeText = computed(() => {
    return dayjs.unix(count.value).format('mm分ss秒');
  });
  // 准备定时器
  // resume  继续启动
  // pause   暂停(清理定时器)
  const { resume, pause } = useIntervalFn(
    () => {
      count.value--; // 倒计时减少
      // 倒计时结束
      if (count.value === 0) {
        pause(); // 停止定时器
      }
    },
    1000, // 间隔为 1 秒
    { immediate: false } // 不需要立即执行
  );

  const start = (startTime: number) => {
    // 如果倒计时没结束，直接退出
    if (count.value !== 0) return;

    // 初始化倒计时
    count.value = startTime;

    // 开启倒计时
    resume();
  };

  // 🚨 封装后记得 return 返回
  return { count, resume, pause, start, countTimeText };
}
