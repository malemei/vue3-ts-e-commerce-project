import { message } from '@/components/XtxUI';
import useStore from '@/store';
import axios, { type Method } from 'axios';
// 创建 axios 实例
const instance = axios.create({
  baseURL: 'http://pcapi-xiaotuxian-front-devtest.itheima.net/',
  timeout: 50000,
});

// 添加请求拦截器
instance.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    // ✅ 在组件外，哪里使用，写哪里(消费前获取)
    const { member } = useStore();
    // 1. 获取token
    const { token } = member.profile;
    // 2. token 和 headers 的非空判断
    if (token && config.headers) {
      // 3. 请求头中携带 token 信息
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  function (error) {
    // 对响应错误做点什么
    if (error.code === 'ERR_NETWORK') {
      // 无网络，错误提示
      message({ type: 'error', text: '亲，换个网络试试~' });
    } else {
      // 有网络，但后端认为有错误，提示后端响应的错误
      message({ type: 'error', text: error.response.data.message });
    }
    // 控制台显示错误
    return Promise.reject(error);
  }
);

// 添加响应拦截器
instance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
  }
);

// 后端返回的接口数据格式
interface ApiRes<T = unknown> {
  msg: string;
  result: T;
}

export default instance;

/**
 * axios 二次封装，整合 TS 类型
 * @param url  请求地址
 * @param method  请求类型
 * @param submitData  对象类型，提交数据
 */
export const http = <T>(method: Method, url: string, submitData?: object) => {
  // 下面的<ApiRes<T>>泛型，，当传入泛型是什么，则函数返回值返回类型就是什么（目前我的理解）
  return instance.request<ApiRes<T>>({
    url,
    method,
    // 🔔 自动设置合适的 params/data 键名称，如果 method 为 get 用 params 传请求参数，否则用 data
    [method.toUpperCase() === 'GET' ? 'params' : 'data']: submitData,
  });
};
