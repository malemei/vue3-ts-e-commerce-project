/* ts文件中分为两种一种是xxx.ts，一种是xxx.d.ts
那么第二种是因为 最终所有文件打包都会形成js文件 然而文件内的类型这些type在打包时会被删除掉，
那么在打包会很尴尬形成一个空文件被打包出去，那他有什么用呢整个空文件出来，
所以我们通过声明xxx.d.ts 告诉他在打包时不会生成js文件，仅在开发时校验提示使用  */

// 统一导出所有types类型文件
export * from './api/home';
export * from './api/goods';
export * from './api/member';
export { QQUserInfo } from './api/qq';
export * from './api/cart';
export * from './api/order';
