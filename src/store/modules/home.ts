import { http } from '@/utils/request';
import { defineStore } from 'pinia';
import type { CategoryList, BannerList, GoodsList } from '@/types';

// 定义一个store
const useHomeStore = defineStore('home', {
  // 开启Store持久化存储
  persist: true,
  // 相当于data
  state: () => ({
    // 通过类型断言，让空数组[]就是那个CategoryList数组，为了以后更方便调用CategoryList中的属性
    categoryList: [] as CategoryList,
    bannerList: [] as BannerList,
    newGoodsList: [] as GoodsList,
    hotGoodsList: [] as GoodsList,
  }),
  // 相当于computed
  getters: {},
  // 相当于methods
  actions: {
    async getAllCategory() {
      // const { data: res } = await request.get('/home/category/head');
      // 封装axios ----> http（）
      const { data: res } = await http<CategoryList>(
        'GET',
        '/home/category/head'
      );
      this.categoryList = res.result;
    },
    // 获取轮播图数据
    async getBannerList() {
      const res = await http<BannerList>('GET', '/home/banner');
      this.bannerList = res.data.result;
    },
    async getNewGoodsList() {
      const res = await http<GoodsList>('GET', '/home/new');
      this.newGoodsList = res.data.result;
    },
    async getHotGoodsList() {
      const res = await http<GoodsList>('GET', '/home/hot');
      this.hotGoodsList = res.data.result;
    },
  },
});

export default useHomeStore;
